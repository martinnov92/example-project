import { ChangeEvent, useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import {
  Button,
  Grid,
  GridItem,
  Heading,
  Input,
  InputGroup,
  InputRightAddon,
  Select,
  Text,
} from "@chakra-ui/react";
import { ArrowLeftIcon } from "@chakra-ui/icons";

import { AppContext } from "../AppContext";
import { RatesKeys } from "../types";
import { calculateExchange, getCurrencySymbol } from "../utils";

export function ExchangeRateForm() {
  const {
    t,
    i18n: { resolvedLanguage },
  } = useTranslation();
  const { rates, selectedRate, setSelectedRate } = useContext(AppContext);
  const exchangeRates = rates[resolvedLanguage as RatesKeys];

  const [value, setValue] = useState("");
  const [result, setResult] = useState("0");

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) =>
    setValue(event.target.value);

  const handleOptionChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const foundRate = exchangeRates.find(
      (exchaneRate) => exchaneRate.code === event.target.value
    );

    setSelectedRate(foundRate);
  };

  const handleCalculate = () => {
    setResult(calculateExchange(value, selectedRate));
  };

  useEffect(() => {
    // clear calculated value on rate change
    setResult("0");
  }, [selectedRate]);

  return (
    <Grid gap={4} alignItems="center" templateColumns="1fr 1fr auto">
      <GridItem>
        <InputGroup>
          <Input
            name="amount"
            type="number"
            value={value}
            bgColor="white"
            placeholder="0.00"
            onChange={handleInputChange}
          />
          <InputRightAddon
            bgColor="white"
            children={<Text>{getCurrencySymbol("CZK", resolvedLanguage)}</Text>}
          />
        </InputGroup>
      </GridItem>

      <GridItem>
        <Select
          bgColor="white"
          value={selectedRate?.code}
          placeholder={t("selectCurrency")}
          onChange={handleOptionChange}
        >
          {exchangeRates?.map((rate) => (
            <option key={rate.code} value={rate.code}>
              {rate.currency} ({getCurrencySymbol(rate.code, resolvedLanguage)})
            </option>
          ))}
        </Select>
      </GridItem>

      <GridItem>
        <Button colorScheme="purple" onClick={handleCalculate}>
          {t("recalculate")}
        </Button>
      </GridItem>

      <GridItem display="flex" justifyContent="center" colSpan={2}>
        <ArrowLeftIcon color="gray.600" transform="rotate(-90deg)" />
      </GridItem>

      <GridItem textAlign="center" colSpan={2}>
        {result ? (
          <Heading noOfLines={1} color="gray.600">
            {new Intl.NumberFormat(resolvedLanguage, {
              style: "currency",
              currency: selectedRate?.code || "CZK",
            }).format(parseFloat(result))}
          </Heading>
        ) : null}
      </GridItem>
    </Grid>
  );
}
