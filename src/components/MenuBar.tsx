import { useTranslation } from "react-i18next";
import { Heading } from "@chakra-ui/react";
import styled from "styled-components";

import { LanguageSwitcher } from ".";

/**
 * style header using styled-components to comply with requirements,
 * otherwise I would use chakra-ui like elsewhere to make it simple for myself :)
 */
const Header = styled.header`
  width: 100%;
  display: flex;
  align-items: center;
  padding: var(--chakra-space-4);
  background-color: var(--chakra-colors-gray-50);
  border-bottom: 1px solid var(--chakra-colors-chakra-border-color);
`;

export function MenuBar() {
  const { t } = useTranslation();

  return (
    <Header>
      <Heading size="md" flex="1">
        {t("exchangeRate")}
      </Heading>

      <LanguageSwitcher />
    </Header>
  );
}
