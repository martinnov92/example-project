import { useTranslation } from "react-i18next";
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
} from "@chakra-ui/react";
import { useContext } from "react";
import { AppContext } from "../AppContext";
import { RatesKeys } from "../types";

const trStyles = { _hover: { backgroundColor: "purple.50" } };

export function ExchangeRatesTable() {
  const {
    t,
    i18n: { resolvedLanguage },
  } = useTranslation();
  const { rates, selectedRate, setSelectedRate } = useContext(AppContext);
  const exchangeRates = rates[resolvedLanguage as RatesKeys];

  return (
    <TableContainer>
      <Table colorScheme="purple" variant="simple" size="sm">
        <Thead>
          <Tr>
            <Th>{t("country")}</Th>
            <Th>{t("currency")}</Th>
            <Th isNumeric>{t("amount")}</Th>
            <Th>{t("code")}</Th>
            <Th isNumeric>{t("rate")}</Th>
          </Tr>
        </Thead>
        <Tbody>
          {exchangeRates?.map((rate) => (
            <Tr
              key={rate.code}
              cursor="pointer"
              backgroundColor={
                rate.code === selectedRate?.code ? "purple.100" : undefined
              }
              sx={trStyles}
              onClick={() => setSelectedRate(rate)}
            >
              <Td>{rate.country}</Td>
              <Td>{rate.currency}</Td>
              <Td isNumeric>{rate.amount}</Td>
              <Td>{rate.code}</Td>
              <Td isNumeric>{rate.rate}</Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </TableContainer>
  );
}
