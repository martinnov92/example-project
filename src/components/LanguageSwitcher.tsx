import { useTranslation } from "react-i18next";
import { Button, Flex, Text } from "@chakra-ui/react";

export function LanguageSwitcher() {
  const { i18n, t } = useTranslation();
  const { resolvedLanguage } = i18n;

  return (
    <Flex align="center" gap={2}>
      <Text>{t("changeLanguage")}:</Text>

      <Button
        size="sm"
        variant="outline"
        colorScheme="purple"
        onClick={() =>
          i18n.changeLanguage(resolvedLanguage === "cs" ? "en" : "cs")
        }
      >
        {resolvedLanguage === "cs" ? t("english") : t("czech")}
      </Button>
    </Flex>
  );
}
