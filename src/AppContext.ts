import { createContext } from "react";

import { AppContextState } from "./types";

export const AppContext = createContext<AppContextState>({
  rates: {
    cs: [],
    en: [],
  },
  selectedRate: undefined,
  setSelectedRate: () => {},
});
