export type ExchangeRate = {
  country: string;
  currency: string;
  amount: number;
  code: string;
  rate: number;
};

export type AppContextState = {
  rates: {
    cs: ExchangeRate[];
    en: ExchangeRate[];
  };
  selectedRate?: ExchangeRate;
  setSelectedRate: (rate?: ExchangeRate) => void;
};

export type RatesKeys = keyof AppContextState["rates"];
