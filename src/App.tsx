import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Box, Flex, Spinner, Text, VStack } from "@chakra-ui/react";
import { WarningTwoIcon } from "@chakra-ui/icons";

import { AppContext } from "./AppContext";
import { ExchangeRateForm, ExchangeRatesTable } from "./components";

import { ExchangeRate } from "./types";
import { fetchExchangeRates } from "./utils";

export function App() {
  const { t } = useTranslation();

  const [hasError, setHasError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [selectedRate, setSelectedRate] = useState<ExchangeRate>();
  const [rates, setRates] = useState<{ [key: string]: ExchangeRate[] }>({
    cs: [],
    en: [],
  });

  useEffect(() => {
    async function downloadExchangeRates() {
      try {
        const [cs, en] = await Promise.all([
          fetchExchangeRates("cs"),
          fetchExchangeRates("en"),
        ]);
        const rates = { cs, en };

        setRates(rates);
      } catch (exception) {
        setHasError(true);
      }

      setIsLoading(false);
    }

    downloadExchangeRates();

    // Run only once
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <AppContext.Provider
      value={{
        rates: { cs: rates.cs, en: rates.en },
        selectedRate,
        setSelectedRate,
      }}
    >
      {isLoading || hasError ? (
        <Flex width="full" height="full" align="center" justify="center">
          {isLoading && (
            <Spinner
              size="xl"
              speed="0.7s"
              thickness="4px"
              color="purple.500"
              emptyColor="purple.100"
            />
          )}

          {hasError && (
            <VStack color="purple.500">
              <WarningTwoIcon fontSize="6xl" />
              <Text fontWeight="bold" fontSize="2xl">
                {t("error")}
              </Text>
            </VStack>
          )}
        </Flex>
      ) : (
        <Flex
          gap={4}
          flex="1"
          height="full"
          align={{ md: "start" }}
          direction={{ base: "column", md: "row" }}
        >
          <Box
            padding={4}
            width="full"
            borderWidth="1px"
            borderRadius="lg"
            backgroundColor="gray.50"
          >
            <ExchangeRateForm />
          </Box>

          <Box
            padding={4}
            width="full"
            overflow="auto"
            borderWidth="1px"
            borderRadius="lg"
          >
            <ExchangeRatesTable />
          </Box>
        </Flex>
      )}
    </AppContext.Provider>
  );
}
