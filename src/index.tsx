import ReactDOM from "react-dom/client";
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import detector from "i18next-browser-languagedetector";
import { Container, ChakraProvider, Flex } from "@chakra-ui/react";

import { App } from "./App";
import { MenuBar } from "./components";
import { cs, en } from "./locales";

i18n.use(detector).use(initReactI18next).init({
  resources: { cs, en },
  fallbackLng: "en",
});

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <ChakraProvider>
    <Flex gap={4} height="100vh" flexDirection="column">
      <MenuBar />

      <Container
        padding={4}
        paddingTop={0}
        minHeight={0}
        height="full"
        maxHeight="full"
        maxWidth="container.xl"
      >
        <App />
      </Container>
    </Flex>
  </ChakraProvider>
);
