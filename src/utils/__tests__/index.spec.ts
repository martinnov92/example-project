import { convertStringToArray } from "..";

// this is part of a response from 'https://www.cnb.cz/en/financial-markets/foreign-exchange-market/central-bank-exchange-rate-fixing/central-bank-exchange-rate-fixing/daily.txt?date=03.06.2022'
const exchangeRatesTxt = `
03 Jun 2022 #108
Country|Currency|Amount|Code|Rate
Australia|dollar|1|AUD|16.690
Brazil|real|1|BRL|4.784
Bulgaria|lev|1|BGN|12.632
Canada|dollar|1|CAD|18.325
China|renminbi|1|CNY|3.458
Croatia|kuna|1|HRK|3.284
`;

test("convertStringToArray", () => {
  expect(convertStringToArray(exchangeRatesTxt)).toEqual([
    {
      code: "AUD",
      country: "Australia",
      currency: "dollar",
      amount: 1,
      rate: 16.69,
    },
    {
      code: "BRL",
      country: "Brazil",
      currency: "real",
      amount: 1,
      rate: 4.784,
    },
    {
      code: "BGN",
      country: "Bulgaria",
      currency: "lev",
      amount: 1,
      rate: 12.632,
    },
    {
      code: "CAD",
      country: "Canada",
      currency: "dollar",
      amount: 1,
      rate: 18.325,
    },
    {
      code: "CNY",
      country: "China",
      currency: "renminbi",
      amount: 1,
      rate: 3.458,
    },
    {
      code: "HRK",
      country: "Croatia",
      currency: "kuna",
      amount: 1,
      rate: 3.284,
    },
  ]);
});
