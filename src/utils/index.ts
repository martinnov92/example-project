import { ExchangeRate, RatesKeys } from "../types";

const EXCHANGE_RATE_RESOURCES = {
  cs: "/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt",
  en: "/en/financial-markets/foreign-exchange-market/central-bank-exchange-rate-fixing/central-bank-exchange-rate-fixing/daily.txt",
};

export async function fetchExchangeRates(language: string) {
  const response = await fetch(EXCHANGE_RATE_RESOURCES[language as RatesKeys]);

  if (!response.ok) {
    throw new Error();
  }

  const text = await response.text();
  const exchangeRates = convertStringToArray(text);

  return exchangeRates;
}

export function convertStringToArray(text: string): ExchangeRate[] {
  if (!text?.length) {
    return [];
  }

  /* get values from 2nd index, because
   * 1st row => date
   * 2nd row => header
   * */
  const rows = text
    .split(/\n/)
    .filter((str) => str?.length)
    .slice(2);

  return rows.map((row) => {
    const [country, currency, amount, code, rate] = row.split("|");
    const exchangeRate: ExchangeRate = {
      code,
      country,
      currency,
      amount: Number(amount),
      rate: Number(rate.replace(",", ".")),
    };

    return exchangeRate;
  });
}

export function calculateExchange(value: string, exchangeRate?: ExchangeRate) {
  const number = parseFloat(value);

  if (isNaN(number) || !exchangeRate?.rate) {
    return "0";
  }

  return (number / exchangeRate.rate).toFixed(2);
}

export function getCurrencySymbol(currency: string, locales = "cs-CZ") {
  return new Intl.NumberFormat(locales, {
    currency,
    style: "currency",
  })
    .formatToParts(0)
    .find(({ type }) => type === "currency")?.value;
}
