# Task: Frontend React application in TypeScript #1

**Description:**

1. Create a simple React application - you can use _Create React App_ - [https://facebook.github.io/create-react-app/](https://facebook.github.io/create-react-app/).
2. After the start the page will fetch the current exchange rate list from the website of the Czech National Bank.
   - API URL: [https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt](https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt)
   - Documentation: [https://www.cnb.cz/cs/casto-kladene-dotazy/Kurzy-devizoveho-trhu-na-www-strankach-CNB/](https://www.cnb.cz/cs/casto-kladene-dotazy/Kurzy-devizoveho-trhu-na-www-strankach-CNB/)
3. List of all exchange rates will be displayed in the table with columns:
   - Country
   - Currency
   - Amount
   - Code
   - Rate
4. Attach a simple form where the user enters the amount in CZK and after pressing the button displays the conversion to the selected currency using the current exchange rate.

**Required dev stack:**

- React (+ Hooks)
- TypeScript
- Styled Components

**Time severity:**

- It shouldn't take more than two hours to complete it.

**Bonus points:**

- Nice commit messages
- Nice graphic design
- Translation to English and other language version
- Tests
- README

**Aditional notes:**

- Best way how to share the code with us is to create new GitHub/GitLab repo.
- You don't have to finish everything. If you don't, just put there a comment describing your intent and we can then discuss it later together.
